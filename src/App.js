import React, { useState } from 'react';
import './App.css';
import OrderDetail from './components/OrderDetail/OrderDetail';
import { nanoid } from 'nanoid';

const App = () => {

  const [mode, setMode] = useState('equal');
  const [people, setPeople] = useState([]);

  const [cost, setCost] = useState({
    tip: 0,
    delivery: 0,
    total: 0,
    peopleNumber: 0
  })



  const onModeChange = e => {
    setMode(e.target.value)
    const costCopy = { ...cost };
    costCopy.total = 0;
    costCopy.peopleNumber = 0;
    setCost(costCopy);
  }

  const addPerson = () => {
    setPeople([...people, { id: nanoid(), name: "", sum: "" }])
  };

  const removePerson = id => {
    const index = people.findIndex(item => item.id === id)
    const peopleCopy = [...people];
    peopleCopy.splice(index, 1)
    setPeople(peopleCopy);
  };

  const onNameChange = (e, i) => {
    const peopleCopy = [...people];
    const personCopy = { ...people[i] }
    personCopy.name = e.target.value;
    peopleCopy[i] = personCopy;
    setPeople(peopleCopy);

  };

  const onSumChange = (e, i) => {
    const peopleCopy = [...people];
    const personCopy = { ...people[i] }
    personCopy.sum = e.target.value;
    peopleCopy[i] = personCopy;
    setPeople(peopleCopy);
  };

  const onTotalChange = e => {
    const costCopy = { ...cost };
    costCopy.total = e.target.value;
    setCost(costCopy);

  };
  const onPeopleNumberChange = e => {
    const costCopy = { ...cost };
    costCopy.peopleNumber = e.target.value;
    setCost(costCopy);

  }
  const onTipChange = e => {
    const costCopy = { ...cost };
    costCopy.tip = e.target.value;
    setCost(costCopy);
  }
  const onDeliveryChange = e => {
    const costCopy = { ...cost };
    costCopy.delivery = e.target.value;
    setCost(costCopy);
  }


  const calculatePayment = () => {
    const total = (((cost.total * cost.tip) / 100) + parseInt(cost.total)) + parseInt(cost.delivery)
    const each = total / cost.peopleNumber
    console.log(total, cost.peopleNumber, each)

  }


  return (
    <div className="App Container">
      <div className="Container">

        <OrderDetail
          mode={mode}
          people={people}
          switchMode={onModeChange}
          add={addPerson}
          remove={removePerson}
          changeName={onNameChange}
          changeSum={onSumChange}
          cost={cost}
          totalChange={onTotalChange}
          peopleNumChange={onPeopleNumberChange}
          tipChange={onTipChange}
          deliveryChange={onDeliveryChange}
        />
        <button type="button" onClick={calculatePayment}>Calculate</button>


      </div>
    </div >
  );
}

export default App;
