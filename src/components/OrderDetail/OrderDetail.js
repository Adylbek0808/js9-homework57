import React from 'react';


const OrderDetail = props => {

  let formType;

  switch (props.mode) {
    case 'equal':
      formType = (
        <div>
          <p>Number of people: <input id="peopleNumber" type="number" placeholder="Number of people" onChange={e => props.peopleNumChange(e)} value={props.cost.peopleNumber}/></p>
          <p>Total order sum: <input id="totalSum" type="number" placeholder="Total sum" onChange={e => props.totalChange(e)} value={props.cost.total}/></p>
        </div>
      );
      break;
    case 'individually':
      const peopleList = props.people.map((person, i) => {
        return (
          <p key={i}>
            <input type="text" placeholder="Enter Name" value={person.name} onChange={e => props.changeName(e, i)} />
            <input className="NumField" type="number" placeholder="Enter sum in numbers" value={person.sum} onChange={e => props.changeSum(e, i)} />
            <button type="button" onClick={() => props.remove(person.id)}>-</button>
          </p>
        );
      });
      formType = (
        <div>
          {peopleList}
          <button type="button" onClick={props.add}>Add people</button>
        </div>
      );
      break;
    default:
      break;
  };



  return (
    <div>
      <p>Choose how to count your order:</p>
      <div>
        <p>
          <input
            type="radio"
            name="options"
            value="equal"
            checked={props.mode === "equal"}
            onChange={props.switchMode}
          /> Count equally between all people
        </p>
        <p>
          <input
            type="radio"
            name="options"
            value="individually"
            checked={props.mode === "individually"}
            onChange={props.switchMode}
          /> Count individually
        </p>
      </div>

      {formType}

      <div>
        <p>Tip percentage: <input id="percentage" type="number" placeholder="default" onChange ={e=> props.tipChange(e)} value = {props.cost.tip}/> %</p>
        <p>Delivery: <input id="delivery" type="number" placeholder="default" onChange ={e=> props.deliveryChange(e)} value = {props.cost.delivery}/> som</p>

      </div>

    </div>
  );
};

export default OrderDetail